-- Options are automatically loaded before lazy.nvim startup
-- Default options that are always set: https://github.com/LazyVim/LazyVim/blob/main/lua/lazyvim/config/options.lua
-- Add any additional options here
vim.o.cursorline = false

vim.opt.guicursor = {
	"n-v-c:block-Cursor/lCursor",
	"i-ci:ver25-Cursor/lCursor",
	"r-cr:hor20-Cursor/lCursor",
	"o:hor50-Cursor",
	"a:blinkwait800-blinkoff500-blinkon1250-Cursor/lCursor",
	"sm:block-Cursor-blinkwait200-blinkoff200-blinkon200",
}

-- relative # lines toggle
vim.opt.relativenumber = false
